import styles from './InputsForRegistration.module.css';
import {useEffect, useState} from "react";

export function InputsForm() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [repeatPassword, setRepeatPassword] = useState('')
    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)
    const [repeatPasswordDirty, setRepeatPasswordDirty] = useState(false)
    const [emailError, setEmailError] = useState('Адрес электронной почты не может быть пустым')
    const [passwordError, setPasswordError] = useState('Пароль не может быть пустым')
    const [repeatPasswordError, setRepeatPasswordError] = useState('Пароль не может быть пустым')
    const[formValid, setFormValid] = useState(true)
    const[register, setRegister] = useState(true)
    useEffect(() => {
        switch((emailDirty && passwordDirty) || (repeatPasswordDirty)) {
            case(true): {
                setFormValid(true);
                break;
            }
            default: {
                setFormValid(false)
            }
        }
    }, [emailDirty, passwordDirty])
    const getTimeOut = () => {
        (setTimeout(() => {
            setRegister(false)
            window.open("/collection");
        }, 3000, 3001))
        return true
    }
    const emailHandler = (e) => {
        setEmail(e.target.value)
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        switch(!re.test(String(e.target.value).toLowerCase())) {
            case(true): {
                setEmailError('Неверные логин или пароль');
                break;
            }
            default: {
                setEmailError('')
            }
        }
    }
    const passwordHandler = (e) => {
        setPassword(e.target.value)
        switch(e.target.value.length < 3 || e.target.value.length > 10) {
            case(true): {
                setPasswordError('Неверные логин или пароль');
                break;
            }
            default: {
                setPasswordError('')
            }
        }
    }
    const repeatPasswordHandler = (e) => {
        setRepeatPassword(e.target.value)
        const password2 = e.target.value
        if (password !== password2) {
            setRepeatPasswordError('Пароли не совпадают')
            return false
        }
        else
            switch((e.target.value.length < 3 || e.target.value.length > 10)) {
                case(true): {
                    setRepeatPasswordError('Неверные пароли');
                    break;
                }
                default: {
                    setRepeatPasswordError('')
                }
            }
    }
    const checkData = () => {
        if (emailError || passwordError || repeatPasswordError) {
            setFormValid(false)
        }
        else {
            window.open("/collection");
        }
    }
    const blurHandler = (e) => {
        switch(e.target.name) {
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
            case 'repeatPassword':
                setRepeatPasswordDirty(true)
                break
        }
    }
    return (
        <form>
            <input onChange={e => emailHandler(e)} value={email} onBlur={e => blurHandler(e)}
                   name="email" type="text" className={styles.InputsForRegistration_address}
                   placeholder="Адрес электронной почты"/>
            <input onChange={e => passwordHandler(e)} value={password} onBlur={e => blurHandler(e)}
                   name="password" type="password" className={styles.InputsForRegistration_password}
                   placeholder="Пароль"/>
            <input onChange={e => repeatPasswordHandler(e)} value={repeatPassword} onBlur={e => blurHandler(e)} name="repeatPassword"
                   type="password" className={styles.InputsForRegistration_RepeatPassword} placeholder="Повторите пароль"/>
            {(emailDirty && passwordDirty && repeatPasswordDirty && !emailError && !repeatPasswordError
                && !passwordError && register && getTimeOut()) ? <div><svg className={styles.preloader}/></div>:
                <button type="submit" className={styles.button_register} disabled={!formValid}
                        onClick={checkData}>Регистрация</button>
            }
            {((emailDirty && passwordDirty && repeatPasswordDirty && emailError))
            && <div style={{color:'red'}}>{emailError}</div>}
            {((emailDirty && passwordDirty && repeatPasswordDirty && emailError))
            && <input onChange={e => emailHandler(e)} value={email} onBlur={e => blurHandler(e)}
                      name="email" type="text" className={styles.InputsForRegistration_address}
                      placeholder="Адрес электронной почты" style={{color:'red'}}/>}
            {((emailDirty && passwordDirty && repeatPasswordDirty && repeatPasswordError))
            && <div className={styles.warning2}>{repeatPasswordError}</div>}
            {((emailDirty && passwordDirty && repeatPasswordDirty && repeatPasswordError))
            && <input onChange={e => repeatPasswordHandler(e)} value={repeatPassword} onBlur={e => blurHandler(e)}
                      name="repeatPassword" type="password" className={styles.InputsForRegistration_RepeatPassword}
                      placeholder="Повторите пароль" style={{color:'red'}}/>}
            {((emailDirty && passwordDirty && repeatPasswordDirty && repeatPasswordError)) &&
            <input onChange={e => passwordHandler(e)} value={password} onBlur={e => blurHandler(e)}
                   name="password" type="password" className={styles.InputsForRegistration_password}
                   placeholder="Пароль" style={{color:'red'}}/>}
        </form>
    );
}

export function LoginQuestion() {
    return (
        <div>
            <div className={styles.login_text}>
                Есть логин для входа? <a href='/login/'>Войти</a>
            </div>
        </div>
    );
}
