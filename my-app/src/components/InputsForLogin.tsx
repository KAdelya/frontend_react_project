import styles from './InputsForLogin.module.css';
import {useEffect, useState} from "react";
import {useNavigate} from "react-router";

export function InputsFormLogin() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)
    const [emailError, setEmailError] = useState('Адрес электронной почты не может быть пустым')
    const [passwordError, setPasswordError] = useState('Пароль не может быть пустым')
    const[formValid, setFormValid] = useState(true)
    const[register, setRegister] = useState(true)
    const navigate = useNavigate();

    useEffect(() => {
        switch((emailDirty || passwordDirty)) {
            case(true): {
                setFormValid(true);
                break;
            }
            default: {
                setFormValid(false)
            }
        }
    }, [emailDirty, passwordDirty])
    const getTimeOut = () => {
        (setTimeout(() => {
            setRegister(false)
            window.open("/collection");
        }, 3000, 3001))
        return true
    }
    const emailHandler = (e) => {
        setEmail(e.target.value)
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        switch(!re.test(String(e.target.value).toLowerCase())) {
            case(true): {
                setEmailError('Неверные логин или пароль');
                break;
            }
            default: {
                setEmailError('')
            }
        }
    }
    const passwordHandler = (e) => {
        setPassword(e.target.value)
        switch(e.target.value.length < 3 || e.target.value.length > 10) {
            case(true): {
                setPasswordError('Неверные логин или пароль');
                break;
            }
            default: {
                setPasswordError('')
            }
        }
    }
    const checkData = () => {
        if (emailError || passwordError) {
            setFormValid(false)
        }
        else {
            window.open("/collection");
        }
    }
    const blurHandler = (e) => {
        switch(e.target.name) {
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
        }
    }
    return (
        <form>
            <input onChange={e => emailHandler(e)} value={email} onBlur={e => blurHandler(e)} name="email" type="text"
                   className={styles.InputsForLogin_address} placeholder="Адрес электронной почты"/>
            <input onChange={e => passwordHandler(e)} value={password} onBlur={e => blurHandler(e)} name="password"
                   type="password" className={styles.InputsForLogin_password} placeholder="Пароль"/>
            {(emailDirty && passwordDirty && !emailError && !passwordError && register && getTimeOut())
                ? <div><svg className={styles.preloader}/></div>:
                <button type="submit" className={styles.button_login} disabled={!formValid} onClick={checkData}>Войти</button>
            }
            {((emailDirty && passwordDirty && passwordError) || (emailDirty && passwordDirty && emailError))
            && <div className={styles.warning}>Неверные логин или пароль</div>}
            {((emailDirty && passwordDirty && passwordError) || (emailDirty && passwordDirty && emailError))
            && <input onChange={e => emailHandler(e)} value={email} onBlur={e => blurHandler(e)} name="email" type="text"
            className={styles.InputsForLogin_address} placeholder="Адрес электронной почты" style={{color:'red'}}/>}
            {((emailDirty && passwordDirty && passwordError) || (emailDirty && passwordDirty && emailError))
            && <input onChange={e => passwordHandler(e)} value={password} onBlur={e => blurHandler(e)} name="password"
            type="password" className={styles.InputsForLogin_password} placeholder="Пароль" style={{color:'red'}}/>}
        </form>
    );
}

export function RegisterQuestion() {
    return (
        <div className={styles.register_text}>
            Ещё не зарегистрированы? <a href='/register/'>Регистрация</a>
        </div>
    );
}
