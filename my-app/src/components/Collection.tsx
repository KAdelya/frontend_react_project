import styles from './Collection.module.css';
import CollectionsList from '../containers/collectionsList';
import Details from '../containers/details';

export function CollectionTop() {
    return (
        <div>
            <div className={styles.rectangle}/>
            <div className={styles.profile}/>
            <div className={styles.search_icon}/>
            <input className={styles.search} type="text" placeholder="Поиск"/>
            <button type="submit" className={styles.logo2}/>
            <button className={styles.button_add}/>
            <button className={styles.button_collection}>Коллекция</button>
            <div className={styles.collection_logo}/>
        </div>
    );
}

export function CollectionBody() {
    return (
        <div>
            <div className={styles.collection_theme}>Коллекция</div>
            <CollectionsList/>
            <Details/>
        </div>
    );
}
