import {Component} from 'react';
import "./collectionsList.css"
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import {select} from '../actions';
import icon_list from "../images/icon_list.svg"
import icon_list2 from "../images/icon_list2.svg"
import arrow from "../images/arrow.svg"
import line_under_collection from "../images/line_under_collection.svg"

class CollectionsList extends Component<any, any>{
    constructor(props) {
        super(props);
        this.state = {
            isShow: false,
            isShow2: false,
            isShow3: false,
            path: icon_list
        }
        this.showList = this.showList.bind(this);
        this.createText = this.createText.bind(this);
        this.createNewButton = this.createNewButton.bind(this)
        this.changeUrl = this.changeUrl.bind(this)
        this.createText2 = this.createText2.bind(this)
    }
    createText() {
        this.setState({ isShow: true });
    }
    createText2() {
        this.setState({ isShow: false });
        this.setState({ isShow3: true });
    }
    createNewButton() {
        this.setState({ isShow2: true });
    }
    changeUrl() {
        switch(this.state.path) {
            case(icon_list): {
                this.setState({path:icon_list2});
                break;
            }
            default: {
                this.setState({path:icon_list});
                break;
            }
        }
    }
    showList() {
        if (this.state.isShow==true){
            switch(this.state.path) {
                case(icon_list): {
                    return this.props.collects.map((collects) => {
                        return (
                            <div>
                                <div onClick={() => this.props.select(collects)} key={collects.id}>
                                    {collects.id in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"] ? <div>
                                        <div className={collects.imag}/>
                                        <div className={collects.grade_image}/>
                                        <div className="minari">Минари</div>
                                        <div className="solstice-text">Солнцестояние</div>
                                        <div className="mank-text">Манк</div>
                                        <div className="girls-text">Девушка, пода…</div>
                                        <div className="earth-text">Земля кочевни…</div>
                                        <div className="father-text">Отец</div>
                                        <div className="one_more-text">Еще по одной</div>
                                        <div className="soul-text">Душа</div>
                                        <div className="godzilla-text">Годзилла</div></div>: ''}
                                </div>
                            </div>
                        )
                    })
                }
                default: {
                    return this.props.collects.map((collects) => {
                        return (
                            <div>
                                <div onClick={() => this.props.select(collects)} key={collects.id}>
                                    {collects.id in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"] ? <div>
                                        <div className={collects.name_for_class}>{collects.name}</div>
                                        <button className={collects.button_arrow_for_class} type="submit"><img src={arrow} alt="arrow"/></button>
                                        <img src={line_under_collection} className={collects.line_under_collection} alt="line_under_collection"/>
                                    </div> : ''}
                                </div>
                            </div>
                        )
                    })
                }
            }
        }
        else if ((this.state.isShow === false) && (this.state.isShow3 === true)) {
            switch (this.state.path) {
                case(icon_list2): {
                    return this.props.collects.map((collects) => {
                        return (
                            <div>
                                <div onClick={ () => this.props.select(collects) } key={ collects.id }>
                                    { (collects.id === "10" || collects.id === "11" || collects.id === "12" ||
                                        collects.id === "13" || collects.id === "14" || collects.id === "15" ||
                                        collects.id === "16" || collects.id === "17" || collects.id === "18")
                                        ? <div>
                                            <div className={ collects.name_for_class }>{ collects.name }</div>
                                            <button className={ collects.button_arrow_for_class } type="submit"><img src={ arrow }
                                                                                                                     alt="arrow"/></button>
                                            <img src={ line_under_collection } className={ collects.line_under_collection }
                                                 alt="line_under_collection"/>
                                        </div> : '' }
                                </div>
                            </div>
                        )
                    })
                }
                default: {
                    return this.props.collects.map((collects) => {
                        return (
                            <div>
                                <div onClick={ () => this.props.select(collects) } key={ collects.id }>
                                    { (collects.id === "10" || collects.id === "11" || collects.id === "12" ||
                                        collects.id === "13" || collects.id === "14" || collects.id === "15" ||
                                        collects.id === "16" || collects.id === "17" || collects.id === "18")
                                        ? <div>
                                            <div className={ collects.imag }/>
                                            <div className={ collects.grade_image }/>
                                            <div className="minari">Довод</div>
                                            <div className="solstice-text">Реинкарнация</div>
                                            <div className="mank-text">Сияние</div>
                                            <div className="girls-text">Никто</div>
                                            <div className="earth-text">Джентльмены</div>
                                            <div className="father-text">Однажды...</div>
                                            <div className="one_more-text">Джокер</div>
                                            <div className="soul-text">Три билборда...</div>
                                            <div className="godzilla-text">Паразиты</div>
                                        </div> : '' }
                                </div>
                            </div>
                        )
                    })
                }
            }
        }
    }
    render () {
        return (
            <div>
                <button className="button-watch" type="submit" onClick={this.createText}>Буду смотреть</button>
                <button className="button-watched" type="submit" onClick={this.createText2}>Просмотрено</button>
                {this.showList()}
                <button className="icon_list" type="submit" onClick={this.changeUrl}><img src={this.state.path} alt="icon_list"/></button>
            </div>
        )
    }
}
function mapStateToProps (state) {
    return {
        collects: state.collects
    };
}
function matchDispatchToProps(dispatch) {
    return bindActionCreators({select: select}, dispatch)
}
export default connect (mapStateToProps, matchDispatchToProps)(CollectionsList);
