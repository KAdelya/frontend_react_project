import { connect } from 'react-redux';
import { Navigate } from 'react-router-dom';


export const Details = (props) => {
    if (!props.collects) {
        return <p></p>;
    }
    return (
        <Navigate to={ '/collection/' + props.collects.id }/>
    );
};

function mapStateToProps(state) {
    return {
        collects: state.active
    };
}

export default connect(mapStateToProps)(Details);
