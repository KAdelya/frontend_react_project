import {combineReducers} from "redux";
import Collections from './collections';
import ActiveCollection from './activeCollection'

const allCollections = combineReducers({
    collects: Collections,
    active: ActiveCollection,
})
export default allCollections;
