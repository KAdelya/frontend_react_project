import logo from './images/logo.svg';
import './App.css';
import {InputsFormLogin, RegisterQuestion} from "./components/InputsForLogin";
import {Route, BrowserRouter as Router, Routes} from "react-router-dom";
import {InputsForm, LoginQuestion} from "./components/InputsForRegistration";
import {CollectionTop, CollectionBody} from "./components/Collection";
import {createStore} from 'redux';
import allCollections from "./reducers";
import {Provider} from 'react-redux';

const store = createStore(allCollections)

export default function App()  {
    return (
        <div className="App">
            <Router>
                <Routes>
                    <Route path="/collection/" element={<CollectionTop/>} />
                </Routes>
            </Router>
            <Provider store={store}>
                <Router>
                    <Routes>
                        <Route path="/collection/" element={<CollectionBody/>} />
                    </Routes>
                </Router>
            </Provider>
            <Router>
                <Routes>
                    <Route path="/login/" element={<img src={logo} className="App-logo" alt="logo"/>} />
                </Routes>
            </Router>
            <Router>
                <Routes>
                    <Route path="/login/" element={<InputsFormLogin/>} />
                </Routes>
            </Router>
            <Router>
                <Routes>
                    <Route path="/login/" element={<RegisterQuestion/>} />
                </Routes>
            </Router>
            <Router>
                <Routes>
                    <Route path="/register/" element={<img src={logo} className="App-logo" alt="logo"/>} />
                </Routes>
            </Router>
            <Router>
                <Routes>
                    <Route path="/register/" element={<InputsForm />} />
                </Routes>
            </Router>
            <Router>
                <Routes>
                    <Route path="/register/" element={<LoginQuestion/>} />
                </Routes>
            </Router>
        </div>
    );
}
