export const select = (collections) => {
    return {
        type: "COLLECT_SELECTED",
        payload: collections
    }
}
